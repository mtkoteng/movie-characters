# Assignment 4 - Movie Characters

## Contributors
**Edris Afzali, Sultan Iljasov, Markus Koteng and Jakkris Thongma**

## The task
The task of this assignment was to create a REST API for handling different operations on characters, movies and franchises. It was developed a Spring Boot application with usage of Hibernate, PostgreSQL, Swagger and Docker.

## Running the application
https://still-spire-94351.herokuapp.com/

## The data
The database should handle three different objects; characters, movies and franchises. The relation between characters and movies is many-to-many, and the relation between movies and franchises is many-to-one.
### Character
- Id 
- First Name
- Last Name
- Alias 
- Gender
- Picture
---
### Movie
- Id
- Title
- Genre
- Release date
- Director
- Picture
- Trailer
---
### Franchise
- Id
- Name
- Description
---
## The endpoints
- Get all characters: `/api/v1/characters`
- Get spesific character: `/api/v1/characters/{id}`
- Post new character: `/api/v1/characters`
- Update character: `/api/v1/characters/{id}`
- Delete character: `/api/v1/characters/{id}`
---
- Get all movies `/api/v1/movies`
- Get specific movie `/api/v1/movies/{id}`
- Post new movie `/api/v1/movies`
- Update movie `/api/v1/movies/{id}`
- Delete movie `/api/v1/movies/{id}`
- Get characters from specific movie: `/api/v1/movies/{id}/characters` 
---
- Get all franchises `/api/v1/franchises`
- Get specific franchise `/api/v1/franchises/{id}`
- Post new franchise `/api/v1/franchises`
- Update franchise `/api/v1/franchises/{id}`
- Delete franchise `/api/v1/franchises/{id}`
- Get movies from specific franchise: `/api/v1/franchises/{id}/movies` 
- Get all characters from specific franchise: `/api/v1/franchises/{id}/characters`
---
## Postman
A Postman collection with requests towards all the endpoints can be found in the postman folder. It can just be imported into postman, and you can make the requests.

## Hibernate
Hibernate handles all the SQL queries in this project. It converts all the objects (in models folder) into database records. It also handles the relation between the objects, and creates the same relation in the database. The database which is used is PostgreSQL. Hibernate do also handle the cascading between the objects. That meens that when a movie is deleted/updated, it is also updated in for the characters and for the franchise.
