package no.noroff.movieCharacters.services;

import no.noroff.movieCharacters.repositories.CharacterRepository;
import no.noroff.movieCharacters.repositories.FranchiseRepository;
import no.noroff.movieCharacters.repositories.MovieRepository;
import no.noroff.movieCharacters.models.Character;
import no.noroff.movieCharacters.models.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/*
 * This class performs various tasks related to table entities
 */
@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    //Fetches every single character from a specific movie
    public List<Character> getCharactersFromMovie(Long id){
        return movieRepository.getOne(id).getCharacters();
    }

    //Fetches every single movie from a specific franchise
    public List<Movie> getMoviesFromFranchise(Long id){
        return franchiseRepository.getOne(id).getMovies();
    }

    //Fetches every single movie from a specific franchise, adds characters from those movies, and then returns built array
    public List<Character> getCharactersFromFranchise(Long id){
        List<Character> characters = new ArrayList<>();
        List<Movie> movies = new ArrayList<>();
        movies = franchiseRepository.getOne(id).getMovies();
        for (Movie one : movies){
            List<Character> tmp = new ArrayList<>();
            tmp = one.getCharacters();
            for (Character ch : tmp) {
                if (!Character.checkExists(ch.getId(), characters)){
                    characters.add(ch);
                }
            }
        }
        return characters;
    }
}

