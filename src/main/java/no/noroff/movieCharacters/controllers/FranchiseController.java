package no.noroff.movieCharacters.controllers;

import no.noroff.movieCharacters.repositories.FranchiseRepository;
import no.noroff.movieCharacters.models.Character;
import no.noroff.movieCharacters.models.Franchise;
import no.noroff.movieCharacters.models.Movie;
import no.noroff.movieCharacters.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * This class handles the endpoints for franchises.
 * It consists of all the CRUD operations for all franchises
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieService movieService;

    //Fetches every single franchise from the database
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises,status);
    }

    //Fetches a specific franchise from the database
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    //Adds a new franchise to the Franchise table in the database
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        Franchise returnFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, status);
    }

    //Updates an existing franchise in the database with new information
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    //Deletes a franchise from the database
    @DeleteMapping("/{id}")
    public ResponseEntity<Franchise> deleteFranchise(@PathVariable Long id){
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            Franchise franchise = franchiseRepository.findById(id).get();
            franchiseRepository.delete(franchise);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(null, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }
    }

    //Fetches every single movie that the specified franchise is associated to
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getAllMoviesFromFranchise(@PathVariable Long id){
        List<Movie> returnFranchise = null;
        HttpStatus status;

        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = movieService.getMoviesFromFranchise(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    //Fetches every single character that the specified franchise is associated to
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getAllCharactersFromFranchise(@PathVariable Long id){
        List<Character> characters = null;
        HttpStatus status;

        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            characters = movieService.getCharactersFromFranchise(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }
}
