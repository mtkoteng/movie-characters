package no.noroff.movieCharacters.controllers;

import no.noroff.movieCharacters.repositories.MovieRepository;
import no.noroff.movieCharacters.models.Character;
import no.noroff.movieCharacters.models.Movie;
import no.noroff.movieCharacters.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * This class handles the endpoints for movies.
 * It consists of all the CRUD operations for all movies
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController
{
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    //Fetches every single movie from the database
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies()
    {
        HttpStatus status;
        List<Movie> movies = movieRepository.findAll();
       if (movies.size() == 0) status = HttpStatus.NO_CONTENT;
       else status = HttpStatus.OK;
       return new ResponseEntity<>(movies, status);
    }

    //Fetches a specific movie from the database
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id)
    {
        HttpStatus status = HttpStatus.NOT_FOUND;
        Movie result = null;
        boolean exists = movieRepository.existsById(id);
        if (exists)
        {
            result = movieRepository.findById(id).get();
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(result, status);
    }

    //Adds a new movie to the database
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie)
    {
        Movie movieToBeAdded = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movieToBeAdded, status);
    }

    //Updates an existing movie in the database with new information
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id,@RequestBody Movie movie)
    {
        Movie movieToBeUpdated = new Movie();
        HttpStatus status;

        if(!id.equals(movie.getId()))
        {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(movieToBeUpdated, status);
        }
        movieToBeUpdated = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(movieToBeUpdated, status);
    }

    //Deletes a specific movie from the database
    @DeleteMapping("/{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable Long id){
        HttpStatus status;
        if (movieRepository.existsById(id))
        {
            //List<Character> characters = character;
            Movie movie = movieRepository.findById(id).get();
            movieRepository.delete(movie);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(null, status);
    }

    //Fetches every single character that's associated with the specified movie
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getCharactersFromMovie(@PathVariable Long id){
        HttpStatus status = HttpStatus.NOT_FOUND;
        List<Character> result = null;
        boolean exists = movieRepository.existsById(id);
        if (exists)
        {
            result = movieService.getCharactersFromMovie(id);
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(result, status);
    }
}
