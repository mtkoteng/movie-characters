package no.noroff.movieCharacters.controllers;

import no.noroff.movieCharacters.repositories.CharacterRepository;
import no.noroff.movieCharacters.models.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
* This class handles the endpoints for characters.
* It consists of all the CRUD operations for the characters
*/
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;

    //Fetching all the characters from the DB
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> data = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    //Fetching a specific character from the DB, returns 404 Not Found if a character
    // with the given id does not exists.
    @GetMapping ("/{id}")
    public ResponseEntity<Character> getSpecificCharacter(@PathVariable Long id){
        HttpStatus status;
        Character character = new Character();
        if (!characterRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(character, status);
        }
        character = characterRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(character, status);
    }

    //Adds a new character to the DB, id should not be specified as it auto increments
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character addCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(addCharacter, status);
    }

    //Updates a given character. Returns 404 Not Found if the character does not exists
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        Character updatedCharacter = new Character();
        HttpStatus status;
        if(!id.equals(character.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(updatedCharacter, status);
        }
        updatedCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(updatedCharacter, status);
    }

    //Deletes a given character from the DB, returns 404 Not Found if the character does not exists.
    @DeleteMapping("/{id}")
    public ResponseEntity<Character> deleteCharacter(@PathVariable Long id){
        HttpStatus status;
        if (characterRepository.existsById(id)){
            Character character = characterRepository.findById(id).get();
            characterRepository.delete(character);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(null, status);
        } else {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }
    }
}
