package no.noroff.movieCharacters.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * This class keeps all our data that's related to a Movie
 */
@Entity
@Table (name = "movie")
public class Movie {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String genre;

    @Column(name = "release_date")
    private Date releaseDate;

    @Column
    private String director;

    @Column
    private String image;

    @Column
    private String trailer;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "franchise_id")
    private Franchise franchise;

    @JsonGetter("franchise")
    public String franchise(){
        if (franchise != null){
            return "/api/v1/franchises/" + franchise.getId();
        } else {
            return null;
        }
    }

    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private List<Character> characters;

    @JsonGetter("characters")
    public List<String> charactersGetter(){
        if (characters != null){
            return characters.stream()
                    .map(character -> {
                        return "/api/v1/characters/" + character.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Movie(Long id, String title, String genre, Date date, String director, String image, String trailer){
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseDate = date;
        this.director = director;
        this.image = image;
        this.trailer = trailer;
    }

    public Movie(){}

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public String getDirector() {
        return director;
    }

    public String getImage() {
        return image;
    }

    public String getTrailer() {
        return trailer;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public List<Character> getCharacters() {
        return characters;
    }
}
