package no.noroff.movieCharacters.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * This class keeps all our data that's related to a Character
 */
@Entity
@Table(name = "character")
public class Character {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "first_name")
    private String firstName;

    @Column (name = "last_name")
    private String lastName;

    @Column (name = "alias")
    private String alias;

    @Column (name = "gender")
    private String gender;

    @Column (name = "picture")
    private String pictureUrl;

    @ManyToMany(mappedBy = "characters")
    public Set<Movie> movies;

    @JsonGetter("movies")
    public List<String> moviesGetter(){
        if (movies != null){
            return movies.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    public Character() {
    }

    public Character(Long id, String firstName, String lastName, String alias, String gender, String pictureUrl) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.alias = alias;
        this.gender = gender;
        this.pictureUrl = pictureUrl;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAlias() {
        return alias;
    }

    public String getGender() {
        return gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public static boolean checkExists(Long id, List<Character> array)
    {
        for (Character one : array)
        {
            if (one.getId().equals(id)) return true;
        }
        return false;
    }
}
