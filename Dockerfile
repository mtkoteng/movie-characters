FROM openjdk:15
VOLUME tmp
ADD target/movieCharacters-0.0.1-SNAPSHOT.jar movieCharacters.jar
ENTRYPOINT ["java", "-jar", "/movieCharacters.jar"]